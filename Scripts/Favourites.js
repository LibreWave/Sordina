namespace Favourites
{
	// btnFavourites
	const btnFavourites = Content.getComponent("btnFavourites");
	btnFavourites.setLocalLookAndFeel(LookAndFeel.iconToggleButton);
	btnFavourites.setControlCallback(onbtnFavouritesControl);

	inline function onbtnFavouritesControl(component, value)
	{
		value ? show() : hide();
	}
	
	// pnlFavouritesContainer
	const pnlFavouritesContainer = Content.getComponent("pnlFavouritesContainer");
	
	pnlFavouritesContainer.setPaintRoutine(function(g)
	{
		var a = this.getLocalBounds(0);
		
		g.fillAll(this.get("bgColour"));
	});
	
	// pnlFavourites
	const pnlFavourites = Content.getComponent("pnlFavourites");
	
	pnlFavourites.setPaintRoutine(function(g)
	{
		var a = this.getLocalBounds(0);
		
		for (i = 0; i < 128; i++)
		{
			g.setColour(i % 2 == 0 ? this.get("itemColour") : this.get("itemColour2"));
			
			var h = this.getHeight() / 127;
			var y = i * h;
			
			g.fillRect([a[0], y, a[2], h]);
			
			g.setColour(this.get("textColour"));
			
			g.setFont("regular", 14);
			g.drawAlignedText((i + 1) + " :", [a[0] + 10, y, a[2], h], "left");
		}
	});

	// Functions
	inline function show()
	{
		pnlFavouritesContainer.showControl(true);
	}
	
	inline function hide()
	{
		pnlFavouritesContainer.showControl(false);
	}
}