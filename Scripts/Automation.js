/*
    Copyright 2023 David Healey

    This file is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This file is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with This file. If not, see <http://www.gnu.org/licenses/>.
*/

namespace Automation
{
	const ah = Engine.createMidiAutomationHandler();
	
	ah.setUpdateCallback(function(obj)
	{
		var excluded = [0, 32];
		
		for (x in obj)
		{
			if (excluded.contains(x.Controller))
				Engine.showMessageBox("Internal Controller", "CC#" + x.Controller + " is used internally by the plugin. Assigning it to a control may cause issues. Please use a different controller number.", 4);
		}	

		storeData(obj);
	});
	
	//! cmbMidiChannel
	const cmbMidiChannel = Content.getComponent("cmbMidiChannel");
	cmbMidiChannel.setLocalLookAndFeel(LookAndFeel.comboBox);
	cmbMidiChannel.setControlCallback(oncmbMidiChannelControl);

	inline function oncmbMidiChannelControl(component, value)
	{
		for (i = 0; i < 17; i++)
			Settings.toggleMidiChannel(i, false);

		Settings.toggleMidiChannel(value - 1, true);
	}
	
	//! btnProgramList - #Todo
	const btnProgramList = Content.getComponent("btnProgramList");
	btnProgramList.setLocalLookAndFeel(LookAndFeel.iconToggleButton);
	btnProgramList.setControlCallback(onbtnProgramListControl);
	
	inline function onbtnProgramListControl(component, value)
	{
		
	}
	
	//! Functions
	inline function storeData(obj)
	{
		local f = FileSystem.getFolder(FileSystem.AppData).getChildFile("controllers.json");

		if (!isDefined(obj) || obj == [])
			return;

		f.writeObject(obj);
	}
	
	inline function loadData()
	{
		local f = FileSystem.getFolder(FileSystem.AppData).getChildFile("controllers.json");
		
		if (!isDefined(f) || !f.isFile())
			return;
			
		local obj = f.loadAsObject();
		
		if (!isDefined(obj))
			return;

		ah.setAutomationDataFromObject(obj);
	}
	
	inline function setControllerNames()
	{
		local controllers = {"0": "Do Not Use", "32": "Do Not Use"};
		
		local labels = [];
	
		for (i = 0; i < 128; i++)
		{
			if (isDefined(controllers[i]))
				labels.push("CC# " + i + " | " + controllers[i]);
			else
				labels.push("CC# " + i);
		}

		ah.setControllerNumberNames("MIDI CC", labels);
	}
		
	//! Calls
	loadData();
	setControllerNames();
}