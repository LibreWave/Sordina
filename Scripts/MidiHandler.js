namespace MidiHandler
{
	// btnMidi
	const btnMidi = Content.getComponent("btnMidi");
	btnMidi.setControlCallback(onbtnMidiControl);
	
	inline function onbtnMidiControl(component, value)
	{
		knbLowNote.showControl(value);
		knbHighNote.showControl(value);
		knbDynamics.showControl(value);
	}
	
	// knbLowNote
	const knbLowNote = Content.getComponent("knbLowNote");
	
	// knbHighNote
	const knbHighNote = Content.getComponent("knbHighNote");	
	
	// knbDynamics
	const knbDynamics = Content.getComponent("knbDynamics");		
	
	// Functions
}