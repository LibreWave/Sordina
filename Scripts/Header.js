/*
    Copyright 2023 David Healey

    This file is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This file is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with This file. If not, see <http://www.gnu.org/licenses/>.
*/

namespace Header
{
	//! pnlHeader
	const pnlHeader = Content.getComponent("pnlHeader");
		
	//! btnSordina
	const btnSordina = Content.getComponent("btnSordina");
	const lafbtnSordina = Content.createLocalLookAndFeel();
	btnSordina.setLocalLookAndFeel(lafbtnSordina);
	btnSordina.setControlCallback(onbtnSordinaControl);
	
	inline function onbtnSordinaControl(component, value)
	{
		if (!value)
			About.show();
	}
	
	lafbtnSordina.registerFunction("drawToggleButton", function(g, obj)
	{
		var a = obj.area;
	
		g.setColour(Colours.withAlpha(obj.textColour, obj.over ? 0.8 - (obj.value * 0.2) : 1.0));
		g.setFontWithSpacing("title", 20, 0.5);		
		g.drawAlignedText(obj.text, a, "left");
	});
}