/*
    Copyright 2023 David Healey

    This file is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This file is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with This file. If not, see <http://www.gnu.org/licenses/>.
*/

namespace About
{
	//! pnlAboutContainer
	const pnlAboutContainer = Content.getComponent("pnlAboutContainer");
	
	pnlAboutContainer.setPaintRoutine(function(g)
	{	
		var a = [pnlAbout.get("x"), pnlAbout.get("y"), pnlAbout.getWidth(), pnlAbout.getHeight()];	

		g.fillAll(this.get("bgColour"));
		g.drawDropShadow([a[0], a[1], a[2], a[3] + 10], Colours.withAlpha(Colours.black, 0.6), 25);
		
	});
	
	pnlAboutContainer.setMouseCallback(function(event)
	{
		if (event.clicked)
			hide();
	});
	
	//! pnlAbout
	const pnlAbout = Content.getComponent("pnlAbout");
	
	pnlAbout.setPaintRoutine(function(g)
	{
		var a = this.getLocalBounds(0);
		
		// Background
		g.setColour(this.get("bgColour"));
		g.fillRoundedRectangle(a, 10);
	
		// Inner frame
		g.setColour(this.get("itemColour"));
		g.fillRoundedRectangle([30, 82, 510, 183], 10);

		// Abstract lines
		g.setGradientFill([this.get("itemColour2"), 200, 82, Colours.withAlpha(this.get("itemColour2"), 0.1), 200, 82 + 183]);
		g.fillPath(Paths.abstractLines, [30, 82, 510, 183]);

		// Project name
		g.setColour(this.get("textColour"));
		g.setFontWithSpacing("title", 20, 0.5);
		g.drawAlignedText(this.get("text"), [30, a[1] + 30, a[2], 15], "left");
		
		// Libre Wave logo
		g.setColour(Colours.withAlpha(this.get("textColour"), 0.4));
		g.fillPath(Paths.icons["logo"], [444, 31, 96, 12]);	
	});
	
	//! btnDocumentation
	const btnDocumentation = Content.getComponent("btnDocumentation");
	btnDocumentation.setLocalLookAndFeel(LookAndFeel.linkButton);
	btnDocumentation.setControlCallback(onbtnDocumentationControl);
	
	inline function onbtnDocumentationControl(component, value)
	{
		if (!value)
			Engine.openWebsite(Engine.getProjectInfo().CompanyURL + "/knowledge-base/user-guides/sordina/");
	}

	//! btnSupport
	const btnSupport = Content.getComponent("btnSupport");
	btnSupport.setLocalLookAndFeel(LookAndFeel.linkButton);
	btnSupport.setControlCallback(onbtnSupportControl);
	
	inline function onbtnSupportControl(component, value)
	{
		if (!value)			
			Engine.openWebsite(Engine.getProjectInfo().CompanyURL + "/my-account/support/");
	}
	
	//! Functions
	inline function hide()
	{
		pnlAboutContainer.showControl(false);
	}
	
	inline function show()
	{
		pnlAboutContainer.showControl(true);
	}
}