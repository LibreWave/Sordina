Content.makeFrontInterface(1000, 700);

Synth.deferCallbacks(true);

include("Paths.js");
include("LookAndFeel.js");
include("Manifest.js");
include("Container.js");
include("Header.js");
include("Display.js");
include("Footer.js");
include("Effects.js");
include("Eq.js");
include("Presets.js");
include("PerformanceLabel.js");
include("Automation.js");
include("About.js");
include("UpdateChecker.js");
include("ZoomHandler.js");
function onNoteOn()
{
	
}
 function onNoteOff()
{
	
}
 function onController()
{
	if (Message.getControllerNumber() == 0)
		return Presets.setBank(Message.getControllerValue());	

	if (Message.getControllerNumber() == 32)
		return Presets.setProgram(Message.getControllerValue());
}
function onTimer()
{
	
}
 function onControl(number, value)
{
	
}
 