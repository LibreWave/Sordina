/*
    Copyright 2023, 2024 David Healey

    This file is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This file is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with This file. If not, see <http://www.gnu.org/licenses/>.
*/

namespace Effects
{
	const sordinaFx = Synth.getEffect("sordinaFx");
	const inputGain = Synth.getEffect("inputGain");
	const outputGain = Synth.getEffect("outputGain");
	const sordinaFxAudio = Synth.getAudioSampleProcessor("sordinaFx");
	const saturator0 = Synth.getEffect("saturator0");
	
	Engine.loadAudioFilesIntoPool();	

	//! btnInit - Reset project to init state
	const btnInit = Content.getComponent("btnInit");
	btnInit.setControlCallback(onbtnInitControl);
	
	inline function onbtnInitControl(component, value)
	{
		if (value)
			return;

		sordinaFxAudio.setFile("");
		sordinaFx.setBypassed(true);
		knbWahWah.set("enabled", false);
		Presets.clearLabel();
		Eq.pnlEqDetails.set("text", "");
	}

	//! fltdB
	const fltdB = Content.getAllComponents("fltdB");
	
	for (x in fltdB)
		x.setLocalLookAndFeel(LookAndFeel.decibelDisplay);	

	//! knbMakeupGain
	const knbMakeupGain = Content.getComponent("knbMakeupGain");

	//! knbGain
	const knbGain = Content.getComponent("knbGain");
	knbGain.setLocalLookAndFeel(LookAndFeel.knob);

	//! knbSaturation
	const knbSaturation = Content.getComponent("knbSaturation");
	knbSaturation.setLocalLookAndFeel(LookAndFeel.knob);
	knbSaturation.setControlCallback(onknbSaturationControl);
	
	inline function onknbSaturationControl(component, value)
	{
		local v = 0.25 * value;
		saturator0.setBypassed(value == 0 || !btnEnable.getValue());		
		saturator0.setAttribute(saturator0.Saturation, v);
	}

	//! knbWahWah
	const knbWahWah = Content.getComponent("knbWahWah");
	knbWahWah.setLocalLookAndFeel(LookAndFeel.wahwahSlider);

	//! btnWahWah
	const btnWahWah = Content.getComponent("btnWahWah");
	btnWahWah.setControlCallback(onbtnWahWahControl);
	
	inline function onbtnWahWahControl(component, value)
	{
		if (!isDefined(sordinaFx.WahWahBypass))
			return;

		knbWahWah.set("enabled", value);		
		sordinaFx.setAttribute(sordinaFx.WahWahBypass, value);
	}

	//! btnEnable
	const btnEnable = Content.getComponent("btnEnable");
	btnEnable.setLocalLookAndFeel(LookAndFeel.enableButton);
	btnEnable.setControlCallback(onbtnEnableControl);

	inline function onbtnEnableControl(component, value)
	{
		Eq.setBypassed(!value);
		sordinaFx.setBypassed(!value || sordinaFxAudio.getFilename() == "");
		inputGain.setBypassed(!value);
		outputGain.setBypassed(!value);
		saturator0.setBypassed(knbSaturation.getValue() == 0 || !value);
	}
	
	//! knbMix
	const knbMix = Content.getComponent("knbMix");
	knbMix.setLocalLookAndFeel(LookAndFeel.knob);
	
	//! Functions
}