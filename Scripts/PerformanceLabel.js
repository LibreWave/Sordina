/*
    Copyright 2023, 2024 David Healey

    This file is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This file is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with This file. If not, see <http://www.gnu.org/licenses/>.
*/

namespace PerformanceLabel
{
	//! pnlPerformance
	const pnlPerformance = Content.getComponent("pnlPerformance");
	
	pnlPerformance.setPaintRoutine(function(g)
	{
		var a = this.getLocalBounds(0);
		
		g.setFont("regular", 15);
		g.setColour(this.get("textColour"));
		g.drawAlignedText("CPU: " + this.get("text") + "%", a, "left");
	});

	pnlPerformance.setTimerCallback(function(g)
	{
		this.set("text", Engine.doubleToString(Engine.getCpuUsage(), 2));
		this.repaint();
	});
	
	pnlPerformance.startTimer(1000);
}