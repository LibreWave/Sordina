/*
    Copyright 2023 David Healey

    This file is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This file is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with This file. If not, see <http://www.gnu.org/licenses/>.
*/

namespace LookAndFeel
{
	Content.setUseHighResolutionForPanels(true);
	
	Engine.loadFontAs("{PROJECT_FOLDER}fonts/Inter-Regular.ttf", "regular");
	Engine.loadFontAs("{PROJECT_FOLDER}fonts/Inter-Medium.ttf", "medium");
	Engine.loadFontAs("{PROJECT_FOLDER}fonts/Inter-SemiBold.ttf", "semibold");
	Engine.loadFontAs("{PROJECT_FOLDER}fonts/Inter-Bold.ttf", "bold");
	Engine.loadFontAs("{PROJECT_FOLDER}fonts/JuliusSansOne-Regular.ttf", "title");

	// empty
	const empty = Content.createLocalLookAndFeel();
	
	empty.registerFunction("drawRotarySlider", function(g, obj)	{});

	// knob
	const knob = Content.createLocalLookAndFeel();
	
	knob.registerFunction("drawRotarySlider", function(g, obj)
	{
		var w = (obj.area[2] > 106 ? 190 : 84);
		var a = [obj.area[2] / 2 - w / 2, obj.area[1], w, obj.area[3]];
		var discSize = w - 20;
		var disc = [obj.area[2] / 2 - discSize / 2, a[1] + 10, discSize, discSize];

		// Arc
		var offset = 2.4;
		var endOffset = -offset + 2 * offset * obj.valueNormalized;
		var thickness = 2;
		var isBidirectional = -1.0 * obj.min == obj.max;
		var strokeStyle = {EndCapStyle: "rounded", Thickness: thickness};
		var diameter = a[2] - thickness;
	
		// Background arc
		var path = Content.createPath();
		g.setColour(obj.itemColour1);
		
		path.addArc([0, 0, 1, 1], -offset, offset);
		
		var pathArea = path.getBounds(diameter);
		pathArea[0] += obj.area[2] / 2 - diameter / 2;
		pathArea[1] += a[1] + 2;
			
		g.drawPath(path, pathArea, strokeStyle);
			
		// Value arc
		path = Content.createPath();	
		g.setColour(Colours.withAlpha(obj.itemColour2, 1.0));
		
		path.addArc([0, 0, 1, 1], (-offset * !isBidirectional), endOffset);
		
		pathArea = path.getBounds(diameter);
		pathArea[0] += obj.area[2] / 2 - diameter / 2;
		pathArea[1] += a[1] + 2;
			
		g.drawPath(path, pathArea, strokeStyle);
					
		// Title/Value text
		g.setFont("medium", 15);
		g.setColour(Colours.withAlpha(obj.textColour, 1.0));

		if (obj.hover || obj.clicked)
			g.drawAlignedText(obj.valueAsText, [a[0], obj.area[1], a[2], a[3]], "centredBottom");
		else
			g.drawAlignedText(obj.text, [a[0], obj.area[1], a[2], a[3]], "centredBottom");

		// Shadow
		var shadowPath = Content.createPath();
		shadowPath.addEllipse(disc);

		var shadowColour = Colours.withAlpha(Colours.black, 0.8);
		g.drawDropShadowFromPath(shadowPath, disc, shadowColour, 26, [0, 12]);
		
		// Body
		g.setGradientFill([0xff131313, disc[2] / 2, disc[1], 0xff212124, disc[2] / 2, disc[1] + disc[2] + (w == 84 ? 3 : 5)]);
		g.fillEllipse([disc[0], disc[1], disc[2], disc[2] + (w == 84 ? 3 : 5)]);
		
		g.setGradientFill([0xff4B4B4C, disc[2] / 2, disc[1], 0xff363638, disc[2] / 2, disc[1] + disc[2]]);
		g.fillEllipse(disc);

		g.setGradientFill([0x1cffffff, disc[0] + disc[2] / 2, disc[1] + disc[3] / 2, 0x0, disc[0] + disc[2], disc[3], true]);
		g.fillEllipse(disc);

		g.setGradientFill([0x1affffff, disc[2] / 2, disc[1], 0x0e6a6a6a, disc[2] / 2, disc[1] + disc[2]]);
		g.fillEllipse(disc);

		g.setGradientFill([0x1affffff, disc[2] / 2, disc[1], 0x0e6a6a6a, disc[2] / 2, disc[1] + disc[2]]);
		g.drawEllipse([disc[0] + 2, disc[1] + 2, disc[2] - 4, disc[2] - 4], 4);
		
		// Value indicator
		g.rotate(endOffset, [obj.area[2] / 2, disc[1] + disc[3] / 2]);
		g.setColour(Colours.lightgrey);
		g.fillEllipse([obj.area[2] / 2 - 2.5, disc[1] + (w == 84 ? 10 : 12), 6, 6]);		
		g.setColour(0xff626263);
		g.drawEllipse([obj.area[2] / 2 - 2.5, disc[1] + (w == 84 ? 10 : 12), 6, 6], 1);
		
		g.rotate(-endOffset, [obj.area[2] / 2, disc[1] + disc[3] / 2]);		
	});
	
	//! wahwahSlider
	const wahwahSlider = Content.createLocalLookAndFeel();
	
	wahwahSlider.registerFunction("drawLinearSlider", function(g, obj)
	{
		var a = obj.area;
		var v = obj.valueNormalized;
		
		g.setColour(Colours.withAlpha(obj.bgColour, 0.05));
		g.fillRect([a[0], a[1], a[2], 4]);

		g.setColour(Colours.withAlpha(obj.bgColour, obj.enabled ? 1.0 : 0.3));
		g.fillRect([a[0], a[1] + 1, a[2] * v, 2]);		

		// Title/Value text
		g.setFont("medium", 15);
		g.setColour(Colours.withAlpha(obj.textColour, obj.enabled ? 1.0 : 0.3));

		if (obj.hover || obj.clicked)
			g.drawAlignedText(obj.valueAsText, [a[0], obj.area[1], a[2], a[3]], "centredBottom");
		else
			g.drawAlignedText(obj.text, [a[0], obj.area[1], a[2], a[3]], "centredBottom");
	});
	
	//! enableButton
	const enableButton = Content.createLocalLookAndFeel();
	
	enableButton.registerFunction("drawToggleButton", function(g, obj)
	{
		var a = obj.area;
		
		g.setColour(Colours.withAlpha(obj.value ? obj.itemColour1 : obj.bgColour, obj.over ? 0.8 - (0.2 * obj.down) : 1.0));
		g.fillEllipse([a[0], a[3] / 2 - 7 / 2, 7, 7]);
		
		g.setColour(Colours.withAlpha(obj.textColour, obj.over ? 0.8 - (0.2 * obj.down) : 1.0 - (0.5 * !obj.value)));
		g.setFont("regular", 15);
		g.drawAlignedText(obj.text, a, "centred");	 
	});
	
	//! iconButton
    const iconButton = Content.createLocalLookAndFeel();
    
    iconButton.registerFunction("drawToggleButton", function(g, obj)
    {
		var a = [obj.area[2] / 2 - 3, obj.area[3] / 2 - 6, 6, 12];

		g.setColour(Colours.withAlpha(obj.itemColour1, obj.over ? 0.8 - (0.2 * obj.down) : 1.0 - (0.2 * (obj.down + !obj.enabled))));
		g.fillPath(Paths.icons[obj.text], a);
    });
    
    //! iconToggleButton
	const iconToggleButton = Content.createLocalLookAndFeel();
	
	iconToggleButton.registerFunction("drawToggleButton", function(g, obj)
	{
		var a = obj.area;

		g.setColour(Colours.withAlpha(obj.itemColour1, obj.over ? 0.8 - (0.2 * obj.down) : 1.0 - (0.2 * (obj.down + !obj.enabled))));
		g.fillPath(Paths.icons[obj.text], a);
	});
    
    //! textButton
    const textButton = Content.createLocalLookAndFeel();
    
    textButton.registerFunction("drawToggleButton", function(g, obj)
    {
    	var a = obj.area;
    	var down = obj.down;
    	
    	g.setColour(obj.bgColour);
		g.setColour(Colours.withAlpha(obj.bgColour, obj.over && obj.enabled ? 0.8 - (0.2 * down) : 1.0 - (0.2 * (down + !obj.enabled))));
		
		if (obj.bgColour != 0)
        	g.fillRoundedRectangle(a, 5);

		g.setColour(Colours.withAlpha(obj.textColour, obj.value ? 1.0 - (0.1 * obj.over) - (0.2 * down) : 0.7 - (0.1 * obj.over) - (0.2 * down)));
		g.setFont("semibold", 16);
		g.drawAlignedText(obj.text, a, "centred");
    });
    
    //! linkButton
    const linkButton = Content.createLocalLookAndFeel();
    
    linkButton.registerFunction("drawToggleButton", function(g, obj)
    {
	    var a = obj.area;
	    var text = obj.text;

	    g.setFont("medium", 14);
	    g.setColour(Colours.withAlpha(obj.textColour, obj.over ? 1.0 - (0.3 * obj.value) : 0.8));
	    
	    g.drawHorizontalLine(a[3] - 2, a[0], a[0] + g.getStringWidth(text));
	    g.drawAlignedText(text, a, "left");
    });

    
    //! decibelDisplay
    const decibelDisplay = Content.createLocalLookAndFeel();
    
    decibelDisplay.registerFunction("drawMatrixPeakMeter", function(g, obj)
    {
    	var a = obj.area;
    	var total;
    	var numActive;
    	
    	for (x in obj.peaks)
    	{
	    	if (x > 0.05)
	    	{
		    	numActive++;
		    	total += x;
	    	}
    	}

    	var avg = total / numActive;
    	var db = Engine.doubleToString(Engine.getDecibelsForGainFactor(avg), 1);

    	g.setColour(db >= 0 && db != "inf" ? obj.itemColour2 : obj.itemColour);
    	g.setFont("regular", 12);
    	g.drawAlignedText(numActive > 0 ? db : "-inf", a, "centred");
    });
    
	//! comboBox
	const comboBox = Content.createLocalLookAndFeel();
	
	comboBox.registerFunction("drawComboBox", function(g, obj)
	{
		var a = obj.area;
				
		g.setColour(Colours.withAlpha(obj.textColour, obj.hover ? 0.8 : 0.6));
		g.setFont("regular", 14);
		g.drawAlignedText("MIDI CH: " + obj.text, a, "left");
		
		g.fillPath(Paths.icons.expandMore, [a[2] - 8, a[3] / 2 - 4 / 2, 8, 5]);
	});
	
	comboBox.registerFunction("drawPopupMenuBackground", function(g, obj)
	{
		g.fillAll(0xff232323);
	});

	comboBox.registerFunction("drawPopupMenuItem", function(g, obj)
	{
		var a = obj.area;
				
		g.fillAll(Colours.withAlpha(Colours.darkgrey, obj.isHighlighted ? 1.0 : 0.0));
		
		g.setFont("regular", 16);
		g.setColour(Colours.withAlpha(Colours.white, obj.isTicked ? 1.0 : 0.8));
		g.drawAlignedText(obj.text, [a[0] + 10, a[1], a[2] - 20, a[3]], "left");
	});
	
	comboBox.registerFunction("getIdealPopupMenuItemSize", function(obj)
	{
		return [25, 25];
	});
}