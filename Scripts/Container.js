/*
    Copyright 2023 David Healey

    This file is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This file is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with This file. If not, see <http://www.gnu.org/licenses/>.
*/

namespace Container
{
	const screenBounds = Content.getScreenBounds(true);

	//! pnlContainer
	const pnlContainer = Content.getComponent("pnlContainer");
	pnlContainer.loadImage("{PROJECT_FOLDER}background1x.png", "background1x");
	pnlContainer.loadImage("{PROJECT_FOLDER}background2x.png", "background2x");
	
	pnlContainer.setPaintRoutine(function(g)
	{
		var a = this.getLocalBounds(0);
		
		if (screenBounds[0] > 1920)		
			g.drawImage("background2x", a, 0, 0);
		else
			g.drawImage("background1x", a, 0, 0);
	});
}