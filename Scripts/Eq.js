/*
    Copyright 2024 David Healey

    This file is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This file is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with This file. If not, see <http://www.gnu.org/licenses/>.
*/

namespace Eq
{
	const dsb = Synth.getDisplayBufferSource("parametricEq0");

	const parametricEq0 = Synth.getEffect("parametricEq0");
		
	Engine.addModuleStateToUserPreset("parametricEq0");
	
	const PARAMETERS = [parametricEq0.Freq, parametricEq0.Q, parametricEq0.Gain];
	
	reg displayValues = [];
	
	//! Broadcasters
	const currentBandWatcher = Engine.createBroadcaster({
	  "id": "currentBandWatcher",
	  "args": ["eventType", "value"],
	  "tags": []
	});
	
	currentBandWatcher.attachToEqEvents(["parametricEq0"], ["BandAdded", "BandSelected", "BandRemoved"], "");
	currentBandWatcher.addListener(eqParameterWatcher, "Update the band", updateBands);
	
	const var eqParameterWatcher = Engine.createBroadcaster({
	  "id": "eqParameterWatcher",
	  "args": ["processor", "parameter", "value"],
	  "tags": []
	});
	
	eqParameterWatcher.setEnableQueue(true);
	eqParameterWatcher.addListener(0, "Update the parameter watcher", updateDisplayValues);
	
	// Broadcaster definition
	const var mouseWatcher = Engine.createBroadcaster({
	  "id": "mouseWatcher",
	  "args": ["component", "event"],
	  "tags": []
	});
	
	// attach to event Type
	mouseWatcher.attachToComponentMouseEvents(["fltEq"], "Clicks Only", "Watch for mouse events");
	
	// attach first listener
	mouseWatcher.addListener(0, "Mouse callback", function(component, event){
		
		if (!event.mouseUp)
			return;
			
		pnlEqDetails.set("text", "");
		pnlEqDetails.repaint();
	});	
	
	//! Look and Feel
	
	//! lafFilter
	const lafFilter = Content.createLocalLookAndFeel();
	
	lafFilter.registerFunction("drawFilterDragHandle", function(g, obj)
	{
		var a = obj.handle;
		a = [a[0] + 2, a[1] + 2, a[2] - 4, a[3] - 4];
		
		g.setColour(Colours.withAlpha(Colours.white, 0.2 + (obj.hover * 0.1) - (!obj.enabled * 0.2)));
		g.fillEllipse(a);
	
		g.setColour(Colours.withAlpha(Colours.white, 0.8 + (obj.hover * 0.2) - (!obj.enabled * 0.3)));
		g.fillEllipse([a[0] + a[2] / 2 - a[2] / 1.5 / 2, a[1] + a[3] / 2 - a[3] / 1.5 / 2, a[2] / 1.5, a[3] / 1.5]);
	});
	
	lafFilter.registerFunction("drawAnalyserPath", function(g, obj)
	{
		var a = obj.area;
		var pa = obj.pathArea;
		
		g.setGradientFill([Colours.white, 0, 0, 0x0, 0, a[3]]);
		g.fillPath(obj.path, pa);
	});
	
	lafFilter.registerFunction("drawAnalyserGrid", function(g, obj) {});
	lafFilter.registerFunction("drawAnalyserBackground", function(g, obj) {});
	
	//! UI Components
	
	//! fltEq
	const fltEq = Content.getComponent("fltEq");
	fltEq.setLocalLookAndFeel(lafFilter);
	
	//! pnlEqDetails
	const pnlEqDetails = Content.getComponent("pnlEqDetails");
	pnlEqDetails.data.values = [];
	
	pnlEqDetails.setPaintRoutine(function(g)
	{
		var a = this.getLocalBounds(0);

		g.setFont("regular", 15);
		g.setColour(this.get("textColour"));
		
		g.drawAlignedText(this.get("text"), a, "left");		
	});
	
	//! Functions
	inline function setBypassed(value)
	{
		parametricEq0.setBypassed(value);
	}
	
	inline function setDetailsVisibility(value)
	{
		pnlEqDetails.showControl(value);
	}
	
	inline function updateBands(unused, value)
	{
		if (value < 0)
			return;

		local freqIndex = parametricEq0.BandOffset * value + parametricEq0.Freq;
		local qIndex = parametricEq0.BandOffset * value + parametricEq0.Q;
		local gainIndex = parametricEq0.BandOffset * value + parametricEq0.Gain;

		eqParameterWatcher.removeAllSources();
		eqParameterWatcher.attachToModuleParameter("parametricEq0", [freqIndex, qIndex, gainIndex], "Update display");
	}
	
	inline function updateDisplayValues(unused, parameter, value)
	{
		local parameterIndex = parameter % parametricEq0.BandOffset;
		local parameterSuffix = ["dB", "Hz", "Q"];
		local text = "";

		displayValues[parameterIndex] = Engine.doubleToString(value, 1);
		
		for (i = 0; i < displayValues.length; i++)
		{
			text += displayValues[i] + parameterSuffix[i];
		
			if (i < displayValues.length - 1)
				text += " | ";
		}	

		pnlEqDetails.set("text", text);
		pnlEqDetails.repaint();
	}
	
	inline function setEqProperties()
	{
		local props = {
		  "BufferLength": 4096,
		  "WindowType": "Blackman Harris",
		  "DecibelRange": [-90.0, 18.0],
		  "UsePeakDecay": false,
		  "UseDecibelScale": true,
		  "YGamma": 0.2,
		  "Decay": 0.6,
		  "UseLogarithmicFreqAxis": true
		};
		
		local dp = dsb.getDisplayBuffer(0);
		dp.setRingBufferProperties(props);
	}
	
	//! Calls
	setEqProperties();
}