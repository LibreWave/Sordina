/*
    Copyright 2023, 2024 David Healey

    This file is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This file is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with This file. If not, see <http://www.gnu.org/licenses/>.
*/


namespace Presets
{
	reg currentPresetFile;
	reg loading = false;
	reg btnActiveValue;
	reg eqState;
	reg midiChannel;

	//! User Preset Handler
	const uph = Engine.createUserPresetHandler();

	uph.setPreCallback(function()
	{
		if (!uph.isInternalPresetLoad())
		{
			btnActiveValue = Effects.btnEnable.getValue();
			eqState = Effects.parametricEq0.exportState();
			midiChannel = Automation.cmbMidiChannel.getValue();
		}			
	});

	uph.setPostCallback(function(presetFile)
	{
		if (!uph.isInternalPresetLoad())
		{
			if (isDefined(btnActiveValue))
			{
				Effects.btnEnable.setValue(btnActiveValue);
				Effects.btnEnable.changed();
			}

			if (isDefined(eqState))
				Effects.parametricEq0.restoreState(eqState);

			if (isDefined(midiChannel))
			{
				Automation.cmbMidiChannel.setValue(midiChannel);
				Automation.cmbMidiChannel.changed();
			}
		}

		currentPresetFile = presetFile;
		updatePresetLabel();
		Automation.loadData();
	});

	//! Look and Feel
	const lafPresetBrowser = Content.createLocalLookAndFeel();
    
    lafPresetBrowser.registerFunction("drawPresetBrowserBackground", function(g, obj)
	{
		var a = obj.area;

		g.setColour(obj.itemColour);
		g.fillRect([a[0], 25, a[2], a[3] - 90]);
	});

    lafPresetBrowser.registerFunction("drawPresetBrowserColumnBackground", function(g, obj)
    {
	    var a = obj.area;

		if (a[2] > 500) // Favorites
			obj.text = "";

	    g.setColour(obj.textColour);
	    g.setFont("medium", 18);
	    g.drawAlignedText(obj.text, [a[0], a[1], a[2], a[3] - 50], "centred");
    });
	
    lafPresetBrowser.registerFunction("drawPresetBrowserListItem", function(g, obj)
    {
        var a = [obj.area[0] + 5, obj.area[1], obj.area[2] - 10, obj.area[3]];
        var col = obj.columnIndex;
		var text = obj.text.replace(".preset").replace("/", " | ");

		if (obj.selected)
		{
			g.setColour(0xffc7ffff);
			g.fillRoundedRectangle([a[0] - 5, a[1], 5, a[3]], {CornerSize: 2, Rounded:[1, 0, 1, 0]});		
		}

		g.setColour(Colours.withAlpha(0xff50676f, obj.hover && !obj.selected ? 0.5 : 1.0));

		if (obj.selected)
			g.fillRoundedRectangle([a[0], a[1], a[2] + 5, a[3]], {CornerSize: 2, Rounded:[0, 1, 0, 1]});
		else if (obj.hover)
			g.fillRoundedRectangle([a[0] - 5, a[1], a[2] + 10, a[3]], 2);

		g.setFont("medium", 18);
		g.setColour(obj.selected ? 0xffc7ffff : Colours.grey);
		
		var xOffset = obj.columnIndex == 2 ? 23 : 8;
		g.drawFittedText(text, [a[0] + xOffset, a[1], a[2] - 16, a[3]], "left", 1, 1.0);
    });

	lafPresetBrowser.registerFunction("drawScrollbar", function(g, obj)
	{
		var a = obj.area;
    	var ha = obj.handle;
    	var w = 5;

    	g.setColour(Colours.withAlpha(0xff323B44, 0.5));
    	g.fillRect([a[0] + a[2] - w, a[1], w, a[3]]);

    	g.setColour(Colours.withAlpha(0xffc7ffff, obj.over ? 0.8 - (0.2 * obj.down) : 1.0));
    	g.fillRect([ha[0] + ha[2] - w, ha[1], w, ha[3]]);
	});
	
	lafPresetBrowser.registerFunction("drawDialogButton", function(g, obj)
	{
		if (obj.text == "Show Favorites")
		{
			var icon = obj.value == 0 ? "heart" : "heartFilled";

			g.setColour(Colours.withAlpha(obj.textColour, obj.over ? 0.5 + obj.value * 0.2 : 0.7 + obj.value * 0.2));
			g.fillPath(Paths.icons[icon], obj.area);
		}
	});

	lafPresetBrowser.registerFunction("createPresetBrowserIcons", function(id)
	{
	    if (id == "favorite_on")
	        return Paths.icons.heartFilled;
	
	    if (id == "favorite_off")
	        return Paths.icons.heart;
	});

	//! fltPresetBrowser
	const fltPresetBrowser = Content.getComponent("fltPresetBrowser");	
	fltPresetBrowser.setLocalLookAndFeel(lafPresetBrowser);

	//! btnPresetBrowser
	const btnPresetBrowser = Content.getComponent("btnPresetBrowser");
	btnPresetBrowser.setLocalLookAndFeel(LookAndFeel.textButton);
	btnPresetBrowser.setControlCallback(onbtnPresetBrowserControl);
	
	inline function onbtnPresetBrowserControl(component, value)
	{
		fltPresetBrowser.showControl(value);
		Eq.setDetailsVisibility(!value);
	}

	//! btnPreset
	const btnPreset = Content.getAllComponents("btnPreset\\d");
	
	for (x in btnPreset)
	{
		x.setControlCallback(onbtnPresetControl);
		x.setLocalLookAndFeel(LookAndFeel.iconButton);
	}
	
	inline function onbtnPresetControl(component, value)
	{
		if (value)
			return;

		local index = btnPreset.indexOf(component);
		
		index == 0 ? Engine.loadPreviousUserPreset(false) : Engine.loadNextUserPreset(false);
	}
	
	//! pnlPresetDetails
	const pnlPresetDetails = Content.getComponent("pnlPresetDetails");
	
	pnlPresetDetails.setPaintRoutine(function(g)
	{
		var a = this.getLocalBounds(0);
		
		g.setFont("regular", 15);
		g.setColour(this.get("textColour"));
		g.drawAlignedText(this.get("text"), a, "centred");
	});

	//! knbBank
	const knbBank = Content.getComponent("knbBank");
	knbBank.set("max", Math.floor(Manifest.programs.length / 127));

	//! knbProgram
	const knbProgram = Content.getComponent("knbProgram");
	knbProgram.setControlCallback(onknbProgramControl);

	inline function onknbProgramControl(component, value)
	{
		local bank = knbBank.getValue();
		local index = value + (bank * 128);

		if (index >= Manifest.programs.length)
			return;

		Engine.loadUserPreset(Manifest.programs[index] + ".preset");
	}

	//! Functions
	inline function clearLabel()
	{
		btnPresetBrowser.set("text", "Click to Begin");
		pnlPresetDetails.set("text", "");
		pnlPresetDetails.repaint();
	}
	
	inline function updatePresetLabel()
	{
		if (!isDefined(currentPresetFile))
			return;
		
		local bank = currentPresetFile.getParentDirectory().getParentDirectory().toString(currentPresetFile.NoExtension);
		local category = currentPresetFile.getParentDirectory().toString(currentPresetFile.NoExtension);
		local name = currentPresetFile.toString(currentPresetFile.NoExtension);

		btnPresetBrowser.set("text", name);
		pnlPresetDetails.set("text", bank + " | " + category + " | " + name);
		pnlPresetDetails.repaint();
	}
	
	inline function checkForMissingDataFile()
	{
		local dataFile = FileSystem.getFolder(FileSystem.AppData).getChildFile("AudioResources.dat");
		
		if (isDefined(dataFile) && dataFile.isFile())
			return;
			
		Engine.showMessageBox("Missing Data", "A data file required by Sordina could not be found. This probably means there was a problem during installation. Please install again using Rhapsody v2.3.1 or later. If the problem persists open a support ticket.", 3);
	}
	
	inline function setBank(value)
	{
		knbBank.setValue(value);
	}
	
	inline function setProgram(value)
	{
		knbProgram.setValue(value);
		knbProgram.changed();
	}
	
	//! Calls
	checkForMissingDataFile();
}
